let container = $('.container')[0];
let peer = $('.peer');
let seed = $('#seed');
let inner = $('.inner')[0];
let outer = $('.outer')[0];
let textbox = $('input')[0];

seed.click(function () {
    $(peer).toggleClass('transition');
    $(inner).addClass('rotation');
    $(outer).addClass('rotation');
    $(textbox).val('');
});


peer.bind('transitionend', (e)=>{
    if (!peer.hasClass('transition')){
        $(inner).removeClass('rotation');
        $(outer).removeClass('rotation');    
    }
});


peer.bind('click', (e)=>{
    let text = $(textbox).val();
    let number = $(e.target).html();
    $(textbox).val(text + number);
});