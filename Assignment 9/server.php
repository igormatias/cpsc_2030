<?php
    include_once './dbconfig.php';

    function get_msg($id){
        $conn = connectToDatabase();
        $id = mysqli_real_escape_string($conn, $id);
        $query = "SELECT msg_ID, name, msg FROM MSG WHERE msg_ID > ".$id;
        $query = mysqli_real_escape_string($conn, $query);
        $result = $conn->query($query, MYSQLI_USE_RESULT );
        $msgs = $result->fetch_all(MYSQLI_ASSOC);
        mysqli_free_result($result);
        mysqli_close($conn);
        return $msgs;
    }

    function save_msg($name, $msg, $timestamp){
        $conn = connectToDatabase();
        $name = mysqli_real_escape_string($conn, $name);
        $msg = mysqli_real_escape_string($conn, $msg);
        $query = "INSERT INTO MSG(name, msg, datetime) VALUES ('$name','$msg', '$timestamp')";
        $result = $conn->query($query);
        mysqli_close($conn);
        return $result;
    }

    function get_last_msg(){
        $conn = connectToDatabase();
        $query = "SELECT msg_ID, name, msg FROM MSG ORDER BY msg_ID DESC LIMIT 10";
        $query = mysqli_real_escape_string($conn, $query);
        $result = $conn->query($query, MYSQLI_USE_RESULT );
        $msgs = $result->fetch_all(MYSQLI_ASSOC);
        mysqli_free_result($result);
        mysqli_close($conn);
        return $msgs;
    }
    
    if ($_SERVER["REQUEST_METHOD"] == "POST"){
        if (isset($_POST['command'])){
            if ($_POST['command'] == 'saveMsg'){
                save_msg($_POST['name'], $_POST['msg'], date('Y-m-d G:i:s'));
            }
            if ($_POST['command'] == 'getMsg'){
                echo json_encode(get_msg($_POST['id']));
            }
            if ($_POST['command'] == 'getLastMsg'){
                echo json_encode(get_last_msg());
            }

        }
    }
    else{
        echo save_msg('testName', 'testing a message', date('Y-m-d G:i:s'));
        //print_r(get_msg(0));
        print_r(get_last_msg());
        echo "testing";
    }
?>