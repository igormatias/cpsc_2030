DROP DATABASE IF EXISTS CHAT;
create DATABASE CHAT;
use CHAT;

create table MSG(
    msg_ID         int NOT NULL AUTO_INCREMENT,
    name           varchar(50),
    msg            text,
    datetime       TIMESTAMP, 
    PRIMARY KEY(msg_ID)
);