const rowClass = "row";
const nameClass = "col-sm-auto";
const messageClass = "col-md-auto";
const getMessage = "getMsg";
const saveMessage = "saveMsg";
const getLastMsg = "getLastMsg"

const messageBox = $('#messageBox');
const send = $('button');
const name = $('#name')[0];
const messageText = $('#message')[0];

let id = 0;
init();
$(send).click((e) => {
    savenewMsg();
});

$(messageText).keypress((e) => {
    if (e.which == 13)
        savenewMsg();
});


function getMessages() {
    $.ajax({
        url: "server.php",
        method: "POST",
        data: {
            command: getMessage,
            id: id
        }
    }).done(function (msg) {
        messageIndex = JSON.parse(msg);
        for (let i = 0; i < messageIndex.length; i++) {
            createMessage(messageIndex[i]);
        }
    });


}

function savenewMsg(){
    $.ajax({
        url: "server.php",
        method: "POST",
        data: {
            command: saveMessage,
            name: name.value,
            msg: messageText.value
        }
    }).done(function () {
        getMessages();
    });
    messageText.value = '';

}

function init(){
    $.ajax({
        url: "server.php",
        method: "POST",
        data: {
            command: getLastMsg
        }
    }).done(function (msg) {
        messageIndex = JSON.parse(msg);
        console.log(msg);
        for (let i = messageIndex.length - 1; i >= 0; i--) {
            createMessage(messageIndex[i]);
        }
        setInterval(() => {
            getMessages();
        }, 1000);        
    });    
}

function createMessage(message) {
    msg_id = Number(message.msg_ID);
    id = id > msg_id? id: msg_id;
    let row = document.createElement("div");
    $(row).addClass(rowClass);


    let name = document.createElement("div");
    $(name).addClass(nameClass);
    $(name).text(message.name)

    let messageDiv = document.createElement("div");
    $(messageDiv).addClass(messageClass);
    $(messageDiv).text(message.msg)


    $(name).appendTo(row);
    $(messageDiv).appendTo(row);
    $(row).prependTo(messageBox);
}
