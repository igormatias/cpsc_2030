<?php
//All database Connection variables.
    define ('DB_USER',"CPSC2030");				                //User - root
    define ('DB_PASSWORD',"CPSC2030");				            //password.
    define ('DB_DATABASE',"chat");		                        //Database - project for now
    define ('DB_SERVER',"localhost");	                        //server address - localhost for now
?>

<?php
function connectToDatabase(){
    $conn = new mysqli(DB_SERVER,DB_USER,DB_PASSWORD,DB_DATABASE);
    if (mysqli_connect_errno())
    {
        die("unable to connect to database");
    }
    mysqli_set_charset($conn, 'utf8');                          //Unicode!
    return $conn;
}

?>