function Persona(name, codeName, persona, arcana, imageUrl){
    this.name =  name;
    this.codeName =  codeName;
    this.persona =  persona;
    this.arcana =  arcana;
    this.imageUrl =  imageUrl;
    this.clastleTeam = false;
}


let persona = [
    new Persona('Akira Kurusu', 'Joker', 'Protagonist', 'The Fool', 'https://vignette.wikia.nocookie.net/megamitensei/images/6/63/Persona_5_Hero.png'),
    new Persona('Ryuji Sakamoto', 'Skull', 'Major Character', 'Chariot', 'https://vignette.wikia.nocookie.net/megamitensei/images/b/bc/Ryuji_Sakamoto.png'),
    new Persona('Ann Takamaki', 'Panther', 'Major Character', 'Lovers', 'https://vignette.wikia.nocookie.net/megamitensei/images/b/be/An_takamaki.png'),
    new Persona('Morgana', 'Mona', 'Major Character', 'Magician', 'https://vignette.wikia.nocookie.net/megamitensei/images/6/68/P5_Morgana_character_artwork.png'),
    new Persona('Yusuke Kitagawa', 'Fox', 'Major Character', 'Emperor', 'https://vignette.wikia.nocookie.net/megamitensei/images/5/5c/Yusuke-Kitagawa.png')];



let personaList = document.getElementById('personaList');
let clastleTeam = document.getElementById('clastleTeam');
let profile = document.getElementById('profile');
let innerHTML = '';
let index = 0;
let personaEvent = [];
persona.map((character)=>{
    let p = document.createElement('p');
    p.className='test';
    p.setAttribute('persona', index);
    p.appendChild(document.createTextNode(character.name))
    personaList.appendChild(p);
    index++;
});


personaList.addEventListener('click', (e) =>{
    let index = e.srcElement.getAttribute('persona');
    if (index != undefined){
        if (!persona[index].clastleTeam){
            console.log(clastleTeam.childNodes.length);
            if (clastleTeam.childNodes.length < 4){
                let p = document.createElement('p');
                persona[index].clastleTeam = true;
                p.setAttribute('persona', index);
                p.appendChild(document.createTextNode(persona[index].name));
                clastleTeam.appendChild(p);
                e.srcElement.setAttribute('castleTeam', true);    
            }
        }
        profile.innerHTML = '<div class="card"><div class="card-header">Name: ' + persona[index].name + '</div>' + 
        '<ul class="list-group list-group-flush">' + 
        '<li class="list-group-item">Code Name: ' + persona[index].codeName + '</li>' + 
        '<li class="list-group-item">Persona: ' + persona[index].persona + '</li>' +
        '<li class="list-group-item">Arcana: ' + persona[index].arcana + '</li></ul>' + 
        '<img class="card-img-bottom" src="'+ persona[index].imageUrl + '"></div>';
    }
})

clastleTeam.addEventListener('click', (e) =>{
    let index = e.srcElement.getAttribute('persona');
    if (index != undefined){
        persona[index].clastleTeam = false;
        clastleTeam.removeChild(e.srcElement);
        let p = personaList.childNodes;
        p[++index].setAttribute('castleTeam', false);
    }
})

