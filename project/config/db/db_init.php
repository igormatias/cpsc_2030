<?php
/*Arquivos de configurações*/
require_once 'config.php'; //Banco de dados


/*Variáveis globais*/
$mysqli = new mysqli(DB_SERVER,DB_USER,DB_PASSWORD,DB_DATABASE); //Objeto de conexão ao banco SQL
if (!$mysqli->set_charset("utf8")) {							//Dados enviados ao banco de dados string serão sempre no formato UTF-8
	echo "Error loading character set utf8";
}

//*********************************************************************************************************************************
//*********************************************************************************************************************************
//*********************************************************************************************************************************
//*********************************************************************************************************************************
echo "*********************************************************************************************************************************<br>";
echo "<p>configuring database...<p>";
echo "*********************************************************************************************************************************<br>";
//*********************************************************************************************************************************
$stmt = $mysqli->prepare("DROP TABLE parents");
if  ($stmt->execute())
{
	echo "DROP TABLE parents OK <br>";
}

$sql_string = "CREATE TABLE IF NOT EXISTS parents (
														_id INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
														last_name VARCHAR(40) NOT NULL,
														first_name VARCHAR(40) NOT NULL,
														phone VARCHAR(13) NOT NULL,
														address_street VARCHAR(80) NOT NULL,
														address_city VARCHAR(40) NOT NULL,
														address_province VARCHAR(2) NOT NULL,
														address_country VARCHAR(40) NOT NULL,
														address_zip VARCHAR(10) NOT NULL,
                                                        email VARCHAR(40) NOT NULL,
														date_create DATETIME,
														date_lastaccess DATETIME
															)ENGINE=MyISAM;";


$stmt = $mysqli->prepare($sql_string);
if ($stmt->execute())
{
	echo "create table parents ok <br>";
}


echo "*********************************************************************************************************************************<br>";
//*********************************************************************************************************************************
$stmt = $mysqli->prepare("DROP TABLE children");
if  ($stmt->execute())
{
	echo "DROP TABLE children OK <br>";
}

$sql_string = "CREATE TABLE IF NOT EXISTS children (
														_id INT(10) UNSIGNED AUTO_INCREMENT,
														_parent_id INT(10) UNSIGNED NOT NULL,
														last_name VARCHAR(40) NOT NULL,
														first_name VARCHAR(40) NOT NULL,
														nickname VARCHAR(40) NOT NULL,
														language1 INT(2) NOT NULL,
														language2 INT(2) NOT NULL,
														gender INT(1) UNSIGNED NOT NULL,
                                                        date_birth DATETIME,
														PRIMARY KEY (_id, _parent_id)
															)ENGINE=MyISAM;";


$stmt = $mysqli->prepare($sql_string);
if ($stmt->execute())
{
	echo "create table children ok <br>";
}

echo "*********************************************************************************************************************************<br>";
//*********************************************************************************************************************************
$stmt = $mysqli->prepare("DROP TABLE language");
if  ($stmt->execute())
{
	echo "DROP TABLE language OK <br>";
}

$sql_string = "CREATE TABLE IF NOT EXISTS language (
														_id INT(2) UNSIGNED AUTO_INCREMENT,
														name VARCHAR(40) NOT NULL,
														PRIMARY KEY (_id)
															)ENGINE=MyISAM;";


$stmt = $mysqli->prepare($sql_string);
if ($stmt->execute())
{
	echo "create table language ok <br>";
}

echo "*********************************************************************************************************************************<br>";
//*********************************************************************************************************************************
$stmt = $mysqli->prepare("DROP TABLE gender");
if  ($stmt->execute())
{
	echo "DROP TABLE gender OK <br>";
}

$sql_string = "CREATE TABLE IF NOT EXISTS gender (
														_id INT(1) UNSIGNED AUTO_INCREMENT,
														name VARCHAR(40) NOT NULL,
														PRIMARY KEY (_id)
															)ENGINE=MyISAM;";


$stmt = $mysqli->prepare($sql_string);
if ($stmt->execute())
{
	echo "create table gender ok <br>";
}

echo "*********************************************************************************************************************************<br>";
echo "*********************************************************************************************************************************<br>";
echo "<p>Filling database...<p>";
echo "*********************************************************************************************************************************<br>";
//*********************************************************************************************************************************
//*********************************************************************************************************************************
echo "<p>Language database...<p><br>";
	$sql_string = "INSERT INTO language (name) VALUES ('English')";
	
	$stmt = $mysqli->prepare($sql_string);
	if ($stmt->execute())
	{
		echo "language table populated with English <br>";
	}
	$sql_string = "INSERT INTO language (name) VALUES ('French')";
	
	$stmt = $mysqli->prepare($sql_string);
	if ($stmt->execute())
	{
		echo "language table populated with French <br>";
	}
	$sql_string = "INSERT INTO language (name) VALUES ('Mandarin')";
	
	$stmt = $mysqli->prepare($sql_string);
	if ($stmt->execute())
	{
		echo "language table populated with Mandarin <br>";
	}
	$sql_string = "INSERT INTO language (name) VALUES ('Cantonese')";
	
	$stmt = $mysqli->prepare($sql_string);
	if ($stmt->execute())
	{
		echo "language table populated with Cantonese <br>";
	}
	$sql_string = "INSERT INTO language (name) VALUES ('Korean')";
	
	$stmt = $mysqli->prepare($sql_string);
	if ($stmt->execute())
	{
		echo "language table populated with Korean <br>";
	}
	$sql_string = "INSERT INTO language (name) VALUES ('Vietnamese')";
	
	$stmt = $mysqli->prepare($sql_string);
	if ($stmt->execute())
	{
		echo "language table populated with Vietnamese <br>";
	}
	$sql_string = "INSERT INTO language (name) VALUES ('Spanish')";
	
	$stmt = $mysqli->prepare($sql_string);
	if ($stmt->execute())
	{
		echo "language table populated with Spanish <br>";
	}
	$sql_string = "INSERT INTO language (name) VALUES ('Italian')";
	
	$stmt = $mysqli->prepare($sql_string);
	if ($stmt->execute())
	{
		echo "language table populated with Italian <br>";
	}
	$sql_string = "INSERT INTO language (name) VALUES ('Portuguese')";
	
	$stmt = $mysqli->prepare($sql_string);
	if ($stmt->execute())
	{
		echo "language table populated with Portuguese <br>";
	}
	$sql_string = "INSERT INTO language (name) VALUES ('German')";
	
	$stmt = $mysqli->prepare($sql_string);
	if ($stmt->execute())
	{
		echo "language table populated with German <br>";
	}
	$sql_string = "INSERT INTO language (name) VALUES ('Arabic')";
	
	$stmt = $mysqli->prepare($sql_string);
	if ($stmt->execute())
	{
		echo "language table populated with Arabic <br>";
	}
	$sql_string = "INSERT INTO language (name) VALUES ('Punjabi')";
	
	$stmt = $mysqli->prepare($sql_string);
	if ($stmt->execute())
	{
		echo "language table populated with Punjabi <br>";
	}
	$sql_string = "INSERT INTO language (name) VALUES ('Tagalog')";
	
	$stmt = $mysqli->prepare($sql_string);
	if ($stmt->execute())
	{
		echo "language table populated with Tagalog <br>";
	}

echo "*********************************************************************************************************************************<br>";
//*********************************************************************************************************************************
//*********************************************************************************************************************************
echo "<p>Gender database...<p><br>";
	$sql_string = "INSERT INTO gender (name) VALUES ('Male')";
	
	$stmt = $mysqli->prepare($sql_string);
	if ($stmt->execute())
	{
		echo "gender table populated with Male <br>";
	}
	$sql_string = "INSERT INTO gender (name) VALUES ('Female')";
	
	$stmt = $mysqli->prepare($sql_string);
	if ($stmt->execute())
	{
		echo "gender table populated with Female <br>";
	}
	$sql_string = "INSERT INTO gender (name) VALUES ('Other')";
	
	$stmt = $mysqli->prepare($sql_string);
	if ($stmt->execute())
	{
		echo "gender table populated with Other <br>";
	}



?>