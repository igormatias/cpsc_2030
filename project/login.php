<!doctype hmtl>
<?php
//Try to connect to the database
require_once './config/db/config.php';
require_once './sendEmail.php';

$mysqli = new mysqli(DB_SERVER,DB_USER,DB_PASSWORD,DB_DATABASE);	//realiza uma tentativa de conex�o ao banco de dados
if (mysqli_connect_errno())			//em caso de erro ao conectar ao banco de dados mysqli_connect_errno() retorna o n�mero do erro, caso n�o de erro retorna 0
{
    die("unable to connect to database");
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $email = $_POST['email'];
    $password = hash("sha256",$_POST['password']);
    $timestamp = date('y-m-d G:i:s');
    if (isset($_POST['subscribe'])){
        $subscribe = 'Y';
    }
    else{
        $subscribe = 'N';
    }
    $stmt = $mysqli->prepare("SELECT ID FROM USER WHERE EMAIL = ?");
    $stmt->bind_param("s", $email);
    $stmt->execute();
	$stmt->store_result();
	if($stmt->num_rows == 0 ){
        $stmt = $mysqli->prepare("INSERT INTO user(email, password, created_date) VALUES(?, ?, ?)");
        $stmt->bind_param("sss", $email, $password, $timestamp);
        if (!$stmt->execute()){
            die("some error occured while inserting a new user!");
        }
        $id = $stmt->insert_id;
        $random = mt_rand();
        $token = hash('sha256',dechex($random).$timestamp);
        $stmt = $mysqli->prepare("INSERT INTO email_check(USER_ID, STRING, GENERATED_DATETIME) VALUES(?, ?, ?)");
        $stmt->bind_param("iss", $id, $token, $timestamp);
        if (!$stmt->execute()){
            die("some error occured while inserting a new user!");
        }
        sendEmail($email, $token);
    }
    else{
		$stmt->bind_result($id);
		$stmt->fetch();
        echo "<script>alert('this email already exists in the database');</script>";
    }
    $stmt->close();
    $mysqli->close();
}

?>

<html lang="en">
    <head>
        <!-- Meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
        <!-- CSS and javascript -->
        <link rel="stylesheet" href="./src/bootstrap-4.1.1-dist/css/bootstrap.min.css">
        <script src="./src/tether-1.3.3/dist/js/tether.min.js" type="text"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
        <script src="./src/js/login.js" defer></script>
    </head>
    <body>
        <div class="jumbotron">
            <form method="post" onsubmit="return validate()">
                <div class="form-group">
                    <label for="InputEmail">Email address</label>
                    <input type="email" class="form-control" id="InputEmail" name="email" aria-describedby="emailHelp" placeholder="Enter email">
                    <small id="blankEmail" class="form-text text-muted" style="padding-top: 5px;">Email can not be left in blank!</small>
                </div>
                <div class="form-group">
                    <label for="InputPassword">Password</label>
                    <input type="password" class="form-control" id="inputPassword1" name="password" placeholder="Password">
                    <small id="blankPassword" class="form-text" style="padding-top: 5px; color: red">The password can not be left in blank!</small>
                </div>
                <div class="form-group">
                    <label for="InputPassword2">Repeat Password</label>
                    <input type="password" class="form-control" id="inputPassword2" placeholder="Password">
                    <small id="passwordMatch" class="form-text" style="padding-top: 5px; color: red">The password does not match!</small>
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" name="subscribe" id="subscribe" value="Y">
                    <label class="form-check-label" for="SubscribeCheck">I want to subscribe</label>
                </div>
                <div class="d-flex justify-content-end">
                    <button type="submit" class="btn btn-primary">Create Login</button>
                </div>
            </form>
        </div>        
    </body>
</html>
