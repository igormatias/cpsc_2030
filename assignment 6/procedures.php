<?php
    require_once './DatabaseConfig.php';

    function get_pokemon($id){
        $conn = connectToDatabase();
        $pokemonID = mysqli_real_escape_string($conn, $id);
        $result = $conn->query("call get_pokemon($pokemonID)", MYSQLI_USE_RESULT );
        $pokemon = $result->fetch_all(MYSQLI_ASSOC);
        mysqli_free_result($result);
        mysqli_close($conn);
        return $pokemon[0];
    }

    function get_pokemon_property($query){
        $conn = connectToDatabase();
        $query = mysqli_real_escape_string($conn, $query);
        $result = $conn->query($query, MYSQLI_USE_RESULT );
        $row = $result->fetch_all(MYSQLI_NUM);
        mysqli_free_result($result);
        mysqli_close($conn);
        $ret = array();
        foreach ($row as $types){                           //It returns an array of arrays, so convert to a single array
            foreach ($types as $type){
                array_push($ret, $type);
            }
        }
        return $ret;
    }

    function get_results($query){
        $conn = connectToDatabase();
        $query = mysqli_real_escape_string($conn, $query);
        $result = $conn->query($query);
        $pokemonList = $result->fetch_all(MYSQLI_NUM);
        mysqli_free_result($result);
        mysqli_close($conn);
        return $pokemonList;        
    }
        
    function get_pokemon_type($id){
        $query = "call get_pokemon_type(".$id.")";
        return get_pokemon_property($query);
    }

    function get_pokemon_type_strong($id){
        $query = "call get_pokemon_type_strong(".$id.")";
        return get_pokemon_property($query);
    }

    function get_pokemon_type_resistant($id){
        $query = "call get_pokemon_type_resistant(".$id.")";
        return get_pokemon_property($query);
    }
    function get_pokemon_type_vulnerable($id){
        $query = "call get_pokemon_type_vulnerable(".$id.")";
        return get_pokemon_property($query);
    }
    function get_pokemon_type_weakness($id){
        $query = "call get_pokemon_type_weakness(".$id.")";
        return get_pokemon_property($query);
    }

    function pokemon_list(){
        $query = "call pokemon_list()";
        return get_results($query);
    }

    function pokemon_list_by_type($id){
        $query = "call pokemon_list_by_type(".$id.")" ;
        return get_results($query);
    }

    function type_list(){
        $query = "call type_list()";
        return get_pokemon_property($query);
    }

    function print_array($array){
        if (count($array) == 0){
            echo "none";
        }
        else{
            for ($i = 0; $i < count($array) - 1; $i++){
                printf("%s, ", $array[$i]);
            }
            printf("%s", $array[$i]);    
        }
    }

    function wrap($tag,$value) {
        return "<$tag>$value</$tag>";
    }

    function wrapIntoHTML($tagStart, $tagEnd,$value) {
        return "<$tagStart>$value</$tagEnd>";
    }

    function createSelectBox($name, $class){
        $values = type_list();
        $ret = "<select size=\"10\" id=\"$name\" class=\"$class\" onchange=\"$name(this.value)\">";
        $id = 1;
        $ret = $ret . wrapIntoHTML("option value=0", "option", "All Pokemons");
        foreach($values as $value){
            $ret = $ret . wrapIntoHTML("option value=$id", "option", $value);
            $id++;
        }
        $ret = $ret . "</select>";
        return $ret;
    }

?>