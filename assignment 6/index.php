<?php
require_once './vendor/autoload.php';
require_once './procedures.php';

function print_type($id){
    $types = get_pokemon_type($id);                         //get pokemon types
    print_array($types);
}



$select_box = new Twig_Filter('createSelectBox', 'createSelectBox');
$print_type = new Twig_Filter('print_type', 'print_type');


$loader = new Twig_Loader_Filesystem('./templates');
//$twig = new Twig_Environment($loader, array('cache' => '/path/to/compilation_cache',));
$twig = new Twig_Environment($loader);

$twig->addFilter($select_box);
$twig->addFilter($print_type);


$pokemon_list = pokemon_list();

echo $twig->render('index.html.twig', array('pokemon_List' => $pokemon_list));




/*
$function = new Twig_Function('test', test());
$twig->addFunction($function);
*/
?>




