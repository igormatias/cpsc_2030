DELIMITER //
CREATE PROCEDURE pokemon_list()
BEGIN
  SELECT PokemonId, PokedexNumber, PokemonName
  FROM Pokemon;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE pokemon_list_with_types()
BEGIN
  SELECT P.PokemonId, PokedexNumber, PokemonName, T.TypeName
  FROM Pokemon P 
  INNER JOIN Pokemon_Type PT ON P.PokemonId = PT.PokemonId 
  INNER JOIN Type T ON PT.TypeId = T.Id
ORDER BY P.PokemonId;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE pokemon_list_by_type
(IN TypeID INT)
  BEGIN
    SELECT P.PokemonId, P.PokedexNumber, P.PokemonName
    FROM Pokemon P
    INNER JOIN Pokemon_Type PT ON PT.PokemonID = P.PokemonID
    WHERE PT.TypeID = TypeID;
  END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE pokemon_list_by_type_or_type
(IN TypeID INT, IN TypeID2 INT)
  BEGIN
    SELECT P.PokemonId, P.PokedexNumber, P.PokemonName
    FROM Pokemon P
    INNER JOIN Pokemon_Type PT ON PT.PokemonID = P.PokemonID
    WHERE PT.TypeID = TypeID
    OR PT.TypeID = TypeID2;
  END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE pokemon_list_by_type_and_type
(IN TypeID INT, IN TypeID2 INT)
  BEGIN
    SELECT P.PokemonId, P.PokedexNumber, P.PokemonName
    FROM Pokemon P
    INNER JOIN Pokemon_Type PT ON PT.PokemonID = P.PokemonID
    WHERE PT.TypeID = TypeID
    AND PT.TypeID = TypeID2;
  END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_pokemon
(IN pokemonID INT)
BEGIN
    SELECT * FROM Pokemon P
    WHERE P.PokemonId = pokemonID;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE type_list()
BEGIN
  SELECT Typename
  FROM Type;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_pokemon_type(IN pokemonID INT)
BEGIN
  SELECT T.Typename FROM Type T, Pokemon_Type PT
  WHERE T.Id = PT.TypeId
  AND PT.PokemonID = PokemonID;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_pokemon_type_strong(IN pokemonID INT)
BEGIN
  SELECT T.TypeName as 'Strong' FROM Type T
  INNER JOIN Strong S ON T.ID = S.TypeID
  INNER JOIN Pokemon_Type PT ON S.ID = PT.TypeID
  WHERE PT.PokemonID = pokemonID;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_pokemon_type_resistant(IN pokemonID INT)
BEGIN
  SELECT T.TypeName as 'Strong' FROM Type T
  INNER JOIN Resistant R ON T.ID = R.TypeID
  INNER JOIN Pokemon_Type PT ON R.ID = PT.TypeID
  WHERE PT.PokemonID = pokemonID;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_pokemon_type_vulnerable(IN pokemonID INT)
BEGIN
  SELECT T.TypeName as 'Strong' FROM Type T
  INNER JOIN Vulnerable V ON T.ID = V.TypeID
  INNER JOIN Pokemon_Type PT ON V.ID = PT.TypeID
  WHERE PT.PokemonID = pokemonID;
END //
DELIMITER ;

DELIMITER //
CREATE PROCEDURE get_pokemon_type_weakness(IN pokemonID INT)
BEGIN
  SELECT T.TypeName as 'Strong' FROM Type T
  INNER JOIN Weakness W ON T.ID = W.TypeID
  INNER JOIN Pokemon_Type PT ON W.ID = PT.TypeID
  WHERE PT.PokemonID = pokemonID;
END //
DELIMITER ;




