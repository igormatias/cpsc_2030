DROP DATABASE IF EXISTS pokedex;
create DATABASE pokedex;
use pokedex;

create table Pokemon(
    PokemonId      int NOT NULL AUTO_INCREMENT,
    pokedexNumber  int,
    PokemonName    varchar(50),
    Hp             int,
    Atk            int,
    Def            int,
    SAt            int,
    SDf            int,
    Spd            int,
    BST            int,
    PRIMARY KEY(PokemonId)
)   CHARACTER SET utf8 COLLATE utf8_unicode_ci;


create table Type(
    Id             int NOT NULL AUTO_INCREMENT,
    TypeName       varchar(50),
    PRIMARY KEY(Id)
) CHARACTER SET utf8 COLLATE utf8_unicode_ci;


create table Pokemon_Type(
    PokemonId      int,
    TypeId         int,
    FOREIGN KEY (PokemonId) REFERENCES Pokemon(PokemonId),
    FOREIGN KEY (TypeId) REFERENCES Type(Id),
    PRIMARY KEY(PokemonId, TypeId)
);


create table Strong(
    Id            int,
    TypeId        int,
    FOREIGN KEY (Id) REFERENCES Type(Id),
    FOREIGN KEY (TypeId) REFERENCES Type(Id),
    PRIMARY KEY(Id, TypeId)
);

create table Weakness(
    Id            int,
    TypeId        int,
    FOREIGN KEY (Id) REFERENCES Type(Id),
    FOREIGN KEY (TypeId) REFERENCES Type(Id),
    PRIMARY KEY(Id, TypeId)
);

create table Resistant(
    Id            int,
    TypeId        int,
    FOREIGN KEY (Id) REFERENCES Type(Id),
    FOREIGN KEY (TypeId) REFERENCES Type(Id),
    PRIMARY KEY(Id, TypeId)
);

create table Vulnerable(
    Id            int,
    TypeId        int,
    FOREIGN KEY (Id) REFERENCES Type(Id),
    FOREIGN KEY (TypeId) REFERENCES Type(Id),
    PRIMARY KEY(Id, TypeId)
);