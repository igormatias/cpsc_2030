use pokedex;
/* Find the top 10 Pokémon ranked by defense */
SELECT pokedexNumber, PokemonName, Def FROM Pokemon ORDER BY Def DESC LIMIT 10;

/* Find All Pokémon who is super effective against electric */
/*The pokemon type needs to be strong against electric*/
SELECT P.pokedexNumber, P.PokemonName, T.TypeName FROM Pokemon P
INNER JOIN Pokemon_Type PT ON P.PokemonId = PT.PokemonId
INNER JOIN Type T ON T.id = PT.TypeId
INNER Join Strong S ON S.Id = PT.TypeId
INNER Join Weakness W ON W.Id = PT.TypeId
Where S.TypeId = (SELECT id from Type where TypeName = 'Electric')
AND PT.TypeId = W.id;



/*Find All Pokémon who is vulnerable fighting and strong against ice*/
SELECT P.pokedexNumber, P.PokemonName, T.TypeName FROM Pokemon P
INNER JOIN Pokemon_Type PT ON P.PokemonId = PT.PokemonId
INNER JOIN Type T ON T.id = PT.TypeId
INNER Join Strong S ON S.Id = PT.TypeId
INNER Join vulnerable V ON V.Id = PT.TypeId
Where S.TypeId = (SELECT id from Type where TypeName = 'Ice')
AND V.TypeId = (SELECT id from Type where TypeName = 'Fighting');

/*Find All Pokémon who has a BST between 300 to 400 who is strong against flying and weak against grass types*/
SELECT P.pokedexNumber, P.PokemonName, T.TypeName, P.BST FROM Pokemon P
INNER JOIN Pokemon_Type PT ON P.PokemonId = PT.PokemonId
INNER JOIN Type T ON T.id = PT.TypeId
INNER Join Strong S ON S.Id = PT.TypeId
INNER Join Weakness W ON W.Id = PT.TypeId
Where S.TypeId = (SELECT id from Type where TypeName = 'Flying')
AND W.TypeId = (SELECT id from Type where TypeName = 'Grass')
AND P.BST BETWEEN 300 AND 400
ORDER BY P.BST DESC;

/*Find the best Pokémon against MewTwo(Note Mew Two is pokemon 150*/
/*Need to be the pokemon that is strong and resist against the mewtwo and the atack needs to be the highest one*/
SELECT P.pokedexNumber, P.PokemonName, T.TypeName, P.Atk FROM Pokemon P
INNER JOIN Pokemon_Type PT ON P.PokemonId = PT.PokemonId
INNER JOIN Type T ON T.id = PT.TypeId
INNER Join Strong S ON S.Id = PT.TypeId
INNER Join Resistant R ON R.Id = PT.TypeId
Where S.TypeId = (SELECT TypeId from Pokemon_Type where PokemonId = (SELECT PokemonId FROM Pokemon Where PokemonName = 'Mewtwo'))
AND R.TypeId = (SELECT TypeId from Pokemon_Type where PokemonId = (SELECT PokemonId FROM Pokemon Where PokemonName = 'Mewtwo'))
ORDER BY P.Atk DESC LIMIT 1;